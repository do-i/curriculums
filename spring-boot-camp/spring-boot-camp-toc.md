# Spring Boot Camp

## Development Environment
* IntelliJ
* Gradle
* Git + github

## Web Framework
* Spring Boot
  * RESTful
  * 3-tier architecture (controller, service, repository)

## Java Open Source Libraries
* Lombok
* JUnit 5
* AssertJ
* Mockito
* SLF4J

## Container
* Docker

## Database and Message Queue
* PostgresSQL
* RabbitMQ





