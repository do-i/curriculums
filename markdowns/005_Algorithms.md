# Part 5: Algorithms


### Hash Table

* HashMap, TreeMap, HashSet, TreeSet, LinkedHashMap

### Searching

This section you will learn various search algorithms and their performance in terms of Big-O space/time complexity

* Linear Search
* Binary Search
* Hash Search

### Sorting

* Insertion Sort
* Selection Sort
* Bubble Sort
* Merge Sort
* Quick Sort

### General

* Iterative vs Recursion
* Binary Search on sorted array of int
* Given an array of unsorted int, return the smallest value
* Given an array of unsorted int, return the index of the largest value
* Given a book e.g.,(https://www.gutenberg.org/files/100/100-0.txt), return a most used word
* Given a book, return 10 most used words
