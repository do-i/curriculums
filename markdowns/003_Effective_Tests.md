# Part 3: Writing Effective Tests

Writing correct good coverage unit tests will save a lot of time in the future.

## JUnit 5

Core library for writing unit tests in Java

## AssertJ Library

Fluent and fun way to write tests assertions

## Mockito Library

Mock and stub dependency calls for complex project