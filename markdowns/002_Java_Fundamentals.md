# Part 2: Java Fundamentals

I use terms `github`, `terminal`, `IntelliJ` in this section. If you use alternative tools, translate these terms with e.g., bitbucket, iterm2, Eclipse, respectively.

## Project Inception

1. Create a repository (fundamentals) in git cloud.
2. Clone the reposiroty using terminal window
3. Create Gradle project in IntelliJ
4. Create a Hello World Java app

Note: Create directories (ws/github) under home directory and create repository folder under `ws/github/fundamentals`.
where `ws` is short for workspace. If you have bitbucket, then `ws/bitbucket/fundamentals`.

## Java Fundamentals

Write a first Java program. And compile and run it.
You will learn how to use tools such as git, JUnit and Gradle build tool along with learning Java syntax and API (Application Programming Interface)

### Variables

    int number = 1;

### Operators

[https://docs.oracle.com/javase/tutorial/java/nutsandbolts/operators.html](https://docs.oracle.com/javase/tutorial/java/nutsandbolts/operators.html)


### Data Types

* primitive vs non-primitive

### Conditional Statements

    if
    if else
    while
    for
    do-while

### Method and Class

* instance method vs static method
* instance variables vs static variables
* constants
* constructors

### Visibility Modifiers

public, protected, package-private, private

### Exception Handling

* throw
* throws
* try-catch
* try-catch with resources


### Java Enums

[https://docs.oracle.com/javase/tutorial/java/javaOO/enum.html](https://docs.oracle.com/javase/tutorial/java/javaOO/enum.html)

## References

[Java Operators](https://docs.oracle.com/javase/tutorial/java/nutsandbolts/operators.html)

[Java Enum](https://docs.oracle.com/javase/tutorial/java/javaOO/enum.html)