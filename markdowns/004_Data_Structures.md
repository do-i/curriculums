# Part 4: Data Structures

## Linear Data Structures

### Arrays

* Single array
* Multidimensional array

### Dynamic List

Linked List

Note: Java API `LinkedList` and `ArrayList` are commonly used data structures in Java applications


### Queue

Example, grocery stores checkout line. FIFO (First In First Out)


### Stack

Example, take out books from a bookcase one book at a time. Every book taken out must be put on a small table. The table has only space to put one book so all books must be stack on top of each other.
After bookcase is an empty, put back one book at a time. You put back book in order of LIFO (Last In First Out).

## Non-Linear Data Structures

### Graph

* Vertices/Nodes and Edges
* There is no root node
* e.g., use case: cities connected by road. Find shortest path etc



### Tree

* Nodes and Edges
* Binary Tree
* Binary Search Tree
* There is always a single root node
* No cyclic
* A node can have N children (except binary tree)

## Reference

[https://medium.com/swlh/data-structures-101-e18fc33579fa](https://medium.com/swlh/data-structures-101-e18fc33579fa)