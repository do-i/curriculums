# Part 1: Development Environment

## Tags

(@Preferred): Unless you have strong preference, I recommend you to install software with this tag. I am more familiar with these software with this tag. I can share some tips and help you troubleshoot some issue you may face.

(@Later): This tag indicates you don't need right away but eventually you may need or may want to explor.

### Operating System

Any major OS is fine.

* Ubuntu (@Preferred)
* OSX
* Windows

If you use Windows, I recommend running Ubuntu as a virtual machine.

### Applications

#### Java 11 or above

One of the following

* Amazon Corretto (@Preferred)
* OpenJDK
* Oracle

I haven't used 17 but I think 17 is the way to go.

#### Integrated Development Environment

* IntelliJ Community Edition (@Preferred)
* Eclipse


#### Text Editor

* Visual Studio Code

#### Docker Container (@Later)

This is not need at a start. At some point we may need containerization to reduce configuration headaches.

#### Database Server (@Later)

There are many types of databases available on the Internet. Each has its own purpose.

As for first database exposure I would recommend SQL database.

* PostgreSQL

#### Database Client (@Later)

You can install when PostgreSQL is installed

* DBeaver

#### Terminal

If you use OSX, `iTerm2`
If you use Windows, `git-bash`
If you use Ubuntu, `Terminator`

#### git

Go to this website [https://git-scm.com/](https://git-scm.com/). Download and install based on your OS type.


#### github or bitbucket or gitlab

I highly recommend you to create an account with at least one of the following cloud based git account.

* github
* bigbucket
* gitlab

The most popular one is `github`. I have both github and bitbucket. I just created gitlab for kicks.
